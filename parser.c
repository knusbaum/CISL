#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lexer.h"
#include "parser.h"
#include "io.h"

l_token t_look;

static void Get_NextTok() {
    Lex_FreeTok(&t_look);
    Lex_NextTok(&t_look);
}

static void Match_Toktype(enum toktype tt) {
    if( t_look.type == tt ) {
        Get_NextTok();
    }
    else {
        Abort("(Line %d): Was expecting %s.", Lex_Currline(), Lex_str_for_toktype(t_look.type));
    }
}

static void Match_Toktype_Tokval(enum toktype tt, char *tokval) {
    if( t_look.type == tt && strcmp(t_look.token, tokval) == 0) {
        Get_NextTok();
    }
    else {
        Abort("(Line %d): Was expecting %s(%s).", Lex_Currline(), Lex_str_for_toktype(t_look.type), tokval);
    }
}

// Parse a global var declaration
static void parse_g_var_decl(variable *v) {
    Match_Toktype(VarSym);

    if(t_look.type != TypeSym) {
        Abort("(Line %d): Expecting type description.", Lex_Currline());
    }
    v->type = t_look.type_desc;


    Get_NextTok();
    if(t_look.type != IdentSym) {
        Abort("(Line %d): Expecting identifier.", Lex_Currline());
    }
    v->name = strdup(t_look.token);
    v->value_type = NONESym;
    
    Get_NextTok();
    if(t_look.type == OpSym && strcmp("=", t_look.token) == 0) {
        Get_NextTok();
        v->value_type = t_look.type;
        switch(t_look.type) {
        case IdentSym:
            v->symname = strdup(t_look.token);
            break;
        case IntSym:
            v->intval = t_look.intval;
            break;
        case StrSym:
            v->strval = strdup(t_look.str);
            break;
        default:
            Abort("(Line %d): Expecting literal or identifier!", Lex_Currline());
        }
        Get_NextTok();
    }
    Match_Toktype_Tokval(OpSym, ";");
}

static void parse_param_list(function *f) {
    while(1) {
        if(t_look.type != TypeSym) return;
        Match_Toktype(TypeSym);
        Match_Toktype(IdentSym);
        
        if(! (t_look.type == OpSym && strcmp(t_look.token, ",") == 0)) {
            return;
        }
        Match_Toktype_Tokval(OpSym, ",");
    }
}

static void parse_var_decl(variable *v) {
    Match_Toktype(VarSym);

    if(t_look.type != TypeSym) {
    }
}

static void parse_statement(statement *s) {
    if(t_look.type == VarSym) {
        variable v;
        parse_var_decl(&v);
    }
}

static void parse_program_block(block *b) {
    Match_Toktype_Tokval(OpSym, "{");
    while(!(t_look.type == OpSym && strcmp(t_look.token, "}") == 0)) {
        statement s;
        parse_statement(&s);
    }
    Match_Toktype_Tokval(OpSym, "}");
}

static void parse_func_decl(function *f) {
    Match_Toktype(DefSym);

    if(t_look.type != TypeSym) {
        Abort("(Line %d): Expecting type name after \"func\".", Lex_Currline());
    }
    Match_Toktype(TypeSym);

    if(t_look.type != IdentSym) {
        Abort("(Line %d): Expecting identifier.", Lex_Currline());
    }
    f->name = strdup(t_look.token);
    Match_Toktype(IdentSym);
    
    Match_Toktype_Tokval(OpSym, "(");
    parse_param_list(f);
    Match_Toktype_Tokval(OpSym, ")");
    parse_program_block(&f->body);
    fprintf(stderr, "Returning!\n");
}

void Parse_Init(FILE *f) {
    Lex_Init(f);
    Get_NextTok();
}

int parse(decl *d) {

    // EBNF: program
    switch(t_look.type) {
    case VarSym:
        d->type = VAR;
        parse_g_var_decl(&d->var);
        break;
    case DefSym:
        d->type = FUNC;
        parse_func_decl(&d->func);
        break;
    case ENDSym:
        fprintf(stderr, "Reached EOF.\n");
        return 0;
        break;
    default:
        Lex_FprintTok(stderr, &t_look);
        Abort("(Line %d): Expected var or function definition.", Lex_Currline());
    }
    return 1;
}

static void destroy_var_decl(variable *v) {
    free(v->name);
    switch(v->value_type) {
    case IdentSym:
        free(v->symname);
        break;
    case StrSym:
        free(v->strval);
        break;
    default:
        break;
    }
}

static void destroy_func_decl(function *f) {
    
}

void destroy_decl(decl *d) {
    switch(d->type) {
    case VAR:
        destroy_var_decl(&d->var);
        break;
    case FUNC:
        destroy_func_decl(&d->func);
        break;
    default:
        break;
    }
}

static void print_var_decl(FILE *f, variable *v) {
    fprintf(stderr, "[VAR Name: %s, Type: %s(%d), Value_Type: %s ",
            v->name, Lex_str_for_basetype(v->type.bt), v->type.indirect_count, Lex_str_for_toktype(v->value_type));
    if(v->value_type == IdentSym) {
        fprintf(stderr, "Value: [VAR %s]]\n", v->symname);
    }
    else if(v->value_type == IntSym) {
        fprintf(stderr, "Value: [INT %d]]\n", v->intval);
    }
    else if(v->value_type == StrSym) {
        fprintf(stderr, "Value: [CHAR* \"%s\"]]\n", v->strval);
    }
    else if(v->value_type == NONESym) {
        fprintf(stderr, "Value: UNDEFINED]\n");
    }
    else {
        Abort("Something really weird happened (parser.c:%d)", __LINE__);
    }
}

static void print_func_decl(FILE *f, function *func) {
    
}

void print_decl(FILE *f, decl *d) {
    switch(d->type) {
    case VAR:
        print_var_decl(f, &d->var);
        break;
    case FUNC:
        print_func_decl(f, &d->func);
        break;
    default:
        break;
    }
}
