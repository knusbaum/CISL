#ifndef LEXER_H
#define LEXER_H


enum toktype {
    IdentSym = -1,
    IfSym = 0,
    WhileSym,
    VarSym,
    DefSym,
    // End Of Keywords
    TypeSym,
    IntSym,
    OpSym,
    StrSym,
    CharSym,
    ENDSym,
    NONESym
};

enum basetype {
    CHAR,
    INT
};

typedef struct {
    enum basetype bt;
    unsigned char indirect_count;
} typedescr;

#define TOKEN_LEN 20
typedef struct {
    enum toktype type;
    //unsigned char indirect_count; // For TypeSym, the number of '*', the amount of indirection.
    union {
        //enum typetype t_type;
        typedescr type_desc;
        char token[TOKEN_LEN];
        char *str;
        int intval;
    };
} l_token;

// f can be NULL, will read from stdin.
void Lex_Init(FILE *f);

void GetType(l_token *t);
void GetInt(l_token *t);
void GetName(l_token *t);
void GetOp(l_token *t);
void GetString(l_token *t);
void Lex_NextTok(l_token *t);
void Lex_FreeTok(l_token *t);

void ChangeGetChar(void (*f)(int *));

unsigned int Lex_Currline();
void Lex_FprintTok(FILE *f, l_token *t);
const char *Lex_str_for_toktype(enum toktype tt);
const char *Lex_str_for_basetype(enum basetype bt);

#endif
