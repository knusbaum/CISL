#ifndef IO_H
#define IO_H

void Error(char * s, ...);
void Abort(char *s, ...);
void Expected(char *s, char * actual);
void ExpectedC(char *s, char actual);
void ExpectedCC(char c, char actual);
void Emit(char *s, ...);
void EmitLn(char *s, ...);
void EmitLnT(char *s, ...);

#endif
