#include <stdlib.h>
#include <string.h>
#include "util.h"

int StrEq(char *s1, char *s2) {
    return !strcmp(s1, s2);
}

ssize_t FindIn(char **table, char *val) {
    ssize_t i = 0;
    for(; *table != NULL; i++) {
        if( StrEq(*(table++), val) ) {
            return i;
        }
    }
    return -1;
}

int ExistsIn(char **table, char *val) {
    return FindIn(table, val) >= 0;
}
