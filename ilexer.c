#include <stdio.h>
#include "lexer.h"
#include "io.h"

int main(void) {
    Lex_Init(NULL);
    l_token t;
    while(Lex_NextTok(&t), t.type != ENDSym) {
        Lex_FprintTok(stdout, &t);
    }
    if(t.type == ENDSym) {
        printf("ENDSym\n");
    }
    return 0;
}
