#include <stdlib.h>
#include "types.h"
#include "util.h"

char * basetypes[] = {"INT", "CHAR", NULL};

int IsType(char *t) {
    return ExistsIn(basetypes, t);
}
