#ifndef UTIL_H
#define UTIL_H

int StrEq(char *s1, char *s2);
ssize_t FindIn(char **Table, char *val);
int ExistsIn(char **Table, char *val);

#endif
