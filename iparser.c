#include <stdio.h>
#include "lexer.h"
#include "parser.h"
#include "io.h"

int main(void) {
//    Lex_Init(NULL);
//    l_token t;
    Parse_Init(stdin);
    
    decl d;
    while(parse(&d)) {
        print_decl(stderr, &d);
        destroy_decl(&d);
    }
    return 0;
}
