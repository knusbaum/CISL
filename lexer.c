#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "lexer.h"
#include "types.h"
#include "util.h"
#include "io.h"

//char Token[TOKEN_LEN];
char TokenUpper[TOKEN_LEN];
////char Lookahead[TOKEN_LEN];
//char * StrTok;
//enum toktype Type;

int Look;

char *keywords[] = { "IF", "WHILE", "VAR", "DEF", NULL };
char *toktypestr[] = {"IF", "WHILE", "VAR", "DEF", "TYPE", "INT", "OP", "STR", "CHAR", "END", "NONE", NULL}; // Don't ever do lookups here. This is just for stringification.
char *basetypes[] = {"CHAR", "INT", NULL};
char *addops[] = { "+", "-", NULL };
char *mulops[] = { "*", "/", NULL };
char *relops[] = { "==", "!=", "<", ">", "<=", ">=", NULL };
char *andops[] = { "&&", NULL };
char *orops[] = { "||", "~", NULL };

char syms[] = "{}()+=-*/!><&|~;,";

FILE *file_to_read;
unsigned int line_no;

int IsWhiteSpace(char c) {
    return (c == '\t' ||
            c == ' ' ||
            c == '\n');
}

int IsOrOp(char *c) {
    return ExistsIn(orops, c);
}

int IsAndOp(char *c) {
    return ExistsIn(andops, c);
}

int IsRelOp(char *c) {
    return ExistsIn(relops, c);
}

int IsMulOp(char *c) {
    return ExistsIn(mulops, c);
}

int IsAddOp(char *c) {
    return ExistsIn(addops, c);
}

int IsOperator(char c) {
    return (strchr(syms, c) != NULL);
}

void _mainGetChar(int *l) {
    *l = fgetc(file_to_read);
}

void (*myGetChar)(int *) = _mainGetChar;

static void GetChar(void) {
    if(Look == '\n') {
        line_no++;
    }
    myGetChar(&Look);
}

void ChangeGetChar(void (*f)(int *)) {
    myGetChar = f;
}

void SkipWhite(void) {
    while(IsWhiteSpace(Look)) {
        GetChar();
    }
}

void Lex_Init(FILE *f) {
    if(f) {
        file_to_read = f;
    }
    file_to_read = stdin;
    GetChar();
    SkipWhite();
}

void Match(char c) {
    if(Look != c) ExpectedCC(c, Look);
    GetChar();
    SkipWhite();
}

void GetName(l_token *t) {
    if(!isalpha(Look)) ExpectedC("Alpha", Look);

    int i;
    for(i = 0; isalnum(Look); i++ ) {
        t->token[i] = Look;
        TokenUpper[i] = toupper(Look);
        GetChar();
    }
    t->token[i] = 0;
    TokenUpper[i] = 0;
    t->type = (enum toktype)FindIn(keywords, TokenUpper);
    SkipWhite();
}

void GetType(l_token *t) {
    GetName(t);
    int t_type = FindIn(basetypes, TokenUpper);
    if(t_type >= 0) {
        t->type = TypeSym;
        t->type_desc.bt = (enum basetype)t_type;
        t->type_desc.indirect_count = 0;
        if(Look == '*') {
            int len = strlen(t->token);
            while(Look == '*') {
                t->type_desc.indirect_count++;
                Match('*');
            }
            //t->token[len++] = 0;
            TokenUpper[len] = 0;
        }
    }
    SkipWhite();
}

void GetInt(l_token *t) {
    if(!isdigit(Look)) ExpectedC("Integer", Look);

    int i;
    for(i = 0; isdigit(Look); i++) {
        t->token[i] = Look;
        TokenUpper[i] = Look;
        GetChar();
    }
    t->token[i] = 0;
    TokenUpper[i] = 0;
    t->type = IntSym;
    sscanf(t->token, "%d", &t->intval);
    SkipWhite();
}

void GetOp(l_token *t) {
    if(! IsOperator(Look)) ExpectedC("Operator", Look);

    int i = 0;

    // Get first char.
    t->token[i] = Look;
    TokenUpper[i] = Look;
    i++;
    GetChar();

    // Get any subsequent chars.
    for(; IsOperator(Look); i++) {
        t->token[i] = Look;
        TokenUpper[i] = Look;
        if(t->token[i] == '(' ||
           t->token[i] == '{' ||
           t->token[i] == ')' ||
           t->token[i] == '}') {
            break;
        }
        GetChar();
    }
    t->token[i] = 0;
    TokenUpper[i] = 0;
    t->type = OpSym;
    SkipWhite();
}

void GetString(l_token *t) {
    Match('"');
    int max_len = 10;
    int currlen = 0;
    char * str = malloc(max_len * sizeof(char));

    while(Look != '"'){
        if(currlen == max_len - 1) {
            max_len *= 2;
            str = realloc(str, max_len * sizeof(char));
        }
        if(Look == '\n') {
            Abort("(Line %d): Unexpected newline in string literal", line_no);
        }
        str[currlen++] = Look;
        GetChar();
    }
    str[currlen] = 0;
    Match('"');
    t->str = str;
    t->type = StrSym;
    SkipWhite();
}

void GetCharacter(l_token *t) {
    GetChar();
    t->token[0] = Look;
    TokenUpper[0] = toupper(Look);
    t->token[1] = 0;
    TokenUpper[1] = 0;
    GetChar();
    GetChar();
    t->type = CharSym;
    SkipWhite();
}

void Lex_NextTok(l_token *t) {
    if(isalpha(Look)) {
        GetType(t);
    }
    else if(isdigit(Look)) {
        GetInt(t);
    }
    else if(IsOperator(Look)) {
        GetOp(t);
    }
    else if(Look == '"') {
        GetString(t);
    }
    else if(Look == '\'') {
        GetCharacter(t);
    }
    else if(Look == EOF) {
        //Type = ENDSym;
        t->type = ENDSym;
    }
    else {
        Abort("(Line %d): Unexpected token: '%c'", line_no, Look);
    }
}

void Lex_FreeTok(l_token *t) {
    if(t->type == StrSym) {
        free(t->str);
    }
}

unsigned int Lex_Currline() {
    return line_no;
}

void Lex_FprintTok(FILE *f, l_token *t) {
    switch(t->type) {
    case IdentSym: fprintf(f, "[%s]: Identity\n", t->token); break;
    case IfSym: fprintf(f, "[%s]: If Sym\n", t->token); break;
    case WhileSym: fprintf(f, "[%s]: While Sym\n", t->token); break;
    case VarSym: fprintf(f, "[%s]: Var Sym\n", t->token); break;
    case DefSym: fprintf(f, "[%s]: Def Sym\n", t->token); break;
    case TypeSym: fprintf(f, "[%s(%d)]: Type\n", basetypes[t->type_desc.bt], t->type_desc.indirect_count); break;
    case IntSym: fprintf(f, "[%d]: Integer\n", t->intval); break;
    case OpSym: fprintf(f, "[%s]: Operator\n", t->token); break;
    case StrSym: fprintf(f, "[%s]: String\n", t->str); break;
    case CharSym: fprintf(f, "[%s]: Character\n", t->token); break;
    default: fprintf(f, "Unknown");
    }
}

const char *Lex_str_for_toktype(enum toktype tt) {
    return toktypestr[tt];
}

const char *Lex_str_for_basetype(enum basetype bt) {
    return basetypes[bt];
}
