#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <string.h>

#include "io.h"

void Error(char * s, ...) {
    va_list args;
    va_start(args, s);
    fprintf(stderr, "Error: ");
    vfprintf(stderr, s, args);
    fprintf(stderr, "\n");
    va_end(args);
    
}

void Abort(char *s, ...) {
    va_list args;
    va_start(args, s);
    fprintf(stderr, "Error: ");
    vfprintf(stderr, s, args);
    fprintf(stderr, "\n");
    va_end(args);
    exit(1);
}

void Expected(char *s, char * actual) {
    char buff[1024];
    snprintf(buff, 1023, "Got: '%s', Expected '%s'.", actual, s);
    Abort(buff);
}

void ExpectedC(char *s, char actual) {
    if(actual == '\n')
    {
        Expected(s, "\\n");
    }
    else {
        char buff[1024];
        snprintf(buff, 1023, "Got: '%c', Expected '%s'.", actual, s);
        Abort(buff);
    }
}

void ExpectedCC(char c, char actual) {
    if(actual == '\n')
    {
        char buff[2];
        buff[0] = c;
        buff[1] = 0;
        Expected(buff, "\\n");
    }
    else {
        char buff[1024];
        snprintf(buff, 1023, "Got: '%c', Expected '%c'.", actual, c);
        Abort(buff);
    }
}

void Emit(char *s, ...) {
    va_list args;
    va_start(args, s);
    vfprintf(stdout, s, args);
    va_end(args);
}

void EmitLn(char *s, ...) {

    va_list args;
    va_start(args, s);
    vfprintf(stdout, s, args);
    fprintf(stdout, "\n");
    va_end(args);
    fflush(stdout);
}

void EmitLnT(char *s, ...) {

    va_list args;
    va_start(args, s);
    fprintf(stdout, "\t");
    vfprintf(stdout, s, args);
    fprintf(stdout, "\n");
    va_end(args);
    fflush(stdout);
}
