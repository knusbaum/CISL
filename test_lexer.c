#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lexer.h"
#include "io.h"
#include "util.h"

int pos = 0;
int len;
char *current = "x if while var def char char ** int int* ** + - <= >= \"Hello, World!\" 'd'";

void GetChar(int *Look) {
    if(pos < len)
        *Look = current[pos++];
    else *Look = EOF;
}

int main(void) {
    len = strlen(current);
    ChangeGetChar(GetChar);

    Lex_Init(NULL);
    l_token t;
    Lex_NextTok(&t);

    if(t.type != IdentSym) Error("Expected IdentSym: %d. Type: %d, Line: %d", IdentSym, t.type, __LINE__);
    Lex_NextTok(&t);

    if(t.type != IfSym) Error("Expected IfSym. Line: %d", __LINE__);
    Lex_NextTok(&t);

    if(t.type != WhileSym) Error("Expected WhileSym. Line: %d", __LINE__);
    Lex_NextTok(&t);

    if(t.type != VarSym) Error("Expected VarSym. Line: %d", __LINE__);
    Lex_NextTok(&t);

    if(t.type != DefSym) Error("Expected DefSym. Line: %d", __LINE__);
    Lex_NextTok(&t);

    if(t.type != TypeSym) Error("Expected TypeSym. Line: %d", __LINE__);
    if(t.type_desc.bt != CHAR) Error("Expected char. Got: %d. Line: %d", t.type_desc.bt, __LINE__);
    Lex_NextTok(&t);

    if(t.type != TypeSym) Error("Expected TypeSym. Line: %d", __LINE__);
    if(!(t.type_desc.bt == CHAR && t.type_desc.indirect_count == 2)) Error("Expected char(2). Got: %s(%d). Line: %d", t.token, t.type_desc.indirect_count, __LINE__);
    Lex_NextTok(&t);

    if(t.type != TypeSym) Error("Expected TypeSym. Line: %d", __LINE__);
    if(t.type_desc.bt != INT) Error("Expected int. Got: %s. Line: %d", t.token, __LINE__);
    Lex_NextTok(&t);

    if(t.type != TypeSym) Error("Expected TypeSym. Line: %d", __LINE__);
    if(!(t.type_desc.bt == INT && t.type_desc.indirect_count == 3)) Error("Expected int(3). Got: %s(%d). Line: %d", t.token, t.type_desc.indirect_count, __LINE__);
    Lex_NextTok(&t);

    if(t.type != OpSym) Error("Expected OpSym. Line: %d", __LINE__);
    if(!StrEq("+", t.token)) Error("Expected +. Got: %s. Line: %d", t.token, __LINE__);
    Lex_NextTok(&t);

    if(t.type != OpSym) Error("Expected OpSym. Line: %d", __LINE__);
    if(!StrEq("-", t.token)) Error("Expected -. Got: %s. Line: %d", t.token, __LINE__);
    Lex_NextTok(&t);

    if(t.type != OpSym) Error("Expected OpSym. Line: %d", __LINE__);
    if(!StrEq("<=", t.token)) Error("Expected <=. Got: %s. Line: %d", t.token, __LINE__);
    Lex_NextTok(&t);

    if(t.type != OpSym) Error("Expected OpSym. Line: %d", __LINE__);
    if(!StrEq(">=", t.token)) Error("Expected >=. Got: %s. Line: %d", t.token, __LINE__);
    Lex_NextTok(&t);

    if(t.type != StrSym) Error("Expected OpSym. Line: %d", __LINE__);
    if(!StrEq(t.str, "Hello, World!")) Error("Expected Hello, World!. Got: %s. Line: %d", t.str, __LINE__);
    Lex_NextTok(&t);

    if(t.type != CharSym) Error("Expected OpSym. Line: %d", __LINE__);
    if(!StrEq(t.token, "d")) Error("Expected d. Got: %s. Line: %d", t.token, __LINE__);
    Lex_NextTok(&t);

    if(t.type != ENDSym) Error("Expected ENDSym. Line: %d", __LINE__);

    printf("Tests complete.\n");

    return 0;
}
