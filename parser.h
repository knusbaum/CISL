

typedef struct {
    char *name;
    typedescr type;
    enum toktype value_type;
    union {
        int intval;
        char *strval;
        char *symname;
    };
} variable;

typedef struct {    
} statement;

typedef struct {
    statement *statements;
} block;

typedef struct {
    char *name;
    block body;
} function;

enum decl_type {
    VAR,
    FUNC
};

typedef struct {
    enum decl_type type;
    union {
        variable var;
        function func;
    };
} decl;

void Parse_Init(FILE *f);
int parse(decl *d);
void destroy_decl(decl *d);
void print_decl(FILE *f, decl *d);
