CC = cc
LD = ld

# types.o
OBJECTS=lexer.o \
	util.o \
	io.o \
	parser.o

CFLAGS = -ggdb -Wall -Werror

COMPNAME = cislc
CISLNAME = cislout

compiler: $(COMPNAME)
test: $(COMPNAME)_ilex_test
ilex: $(COMPNAME)_ilex
iparse: $(COMPNAME)_iparse

all: compiler test ilex iparse

-include $(OBJECTS:.o=.d)

$(COMPNAME): main.o $(OBJECTS)
	@$(CC) $(CFLAGS) $(OBJECTS) main.o -o $(COMPNAME)
	-@echo -e [LD] '\t' $(COMPNAME)

$(COMPNAME)_ilex: ilexer.o $(OBJECTS)
	@$(CC) $(CFLAGS) $(OBJECTS) ilexer.o -o $(COMPNAME)_ilex
	-@echo -e [LD] '\t' $(COMPNAME)_ilex

$(COMPNAME)_ilex_test: test_lexer.o $(OBJECTS)
	@$(CC) $(CFLAGS) $(OBJECTS) test_lexer.o -o $(COMPNAME)_ilex_test
	-@echo -e [LD] '\t' $(COMPNAME)_ilex_test

$(COMPNAME)_iparse: iparser.o $(OBJECTS)
	@$(CC) $(CFLAGS) $(OBJECTS) iparser.o -o $(COMPNAME)_iparse
	-@echo -e [LD] '\t' $(COMPNAME)_iparse

%.o : %.c %.h
	@$(CC) $(CFLAGS) -o $@ -c $<
	@$(CC) -MM $(CFLAGS) -c $< > $*.d
	-@echo -e [CC] '\t' $@

%.o : %.c
	@$(CC) $(CFLAGS) -o $@ -c $<
	@$(CC) -MM $(CFLAGS) -c $< > $*.d
	-@echo -e [CC] '\t' $@

as :
	$(CC) -ggdb -c -m32 test.s
	$(LD) -melf_i386 test.o -o $(CISLNAME)

clean:
	-@rm *~ *.o *.d *.gch

nuke: clean
	-@rm $(COMPNAME) $(CISLNAME) $(COMPNAME)_ilex_test $(COMPNAME)_ilex $(COMPNAME)_iparse
